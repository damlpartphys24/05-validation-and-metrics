{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Higgs Challenge Example\n",
    "\n",
    "(based on [HiggsChallenge.ipynb from LMU block course](https://github.com/nikoladze/LMU_DA_ML/blob/master/HiggsChallenge.ipynb))\n",
    "\n",
    "We will use the dataset from the **[Higgs Boson ML Challenge](https://www.kaggle.com/c/Higgs-boson)** since it shows a few peculiarities often encountered in particle physics applications.\n",
    "\n",
    "* The data is available from **[CERN Open Data](http://opendata.cern.ch/record/328)**.\n",
    "  * more information about the data is available from the links, and in particular in the accompanying **[documentation](http://opendata.cern.ch/record/329/files/atlas-higgs-challenge-2014.pdf)**.\n",
    "  * much of the description below is taken from this documentation\n",
    "* The general idea is that we want to extract $H\\to\\tau^+\\tau^-$ signal from background. \n",
    "  * first channel where coupling of Higgs boson to fermions can be proven (before only coupling to bosons, $\\gamma$, $W$, $Z$)\n",
    "  * by now seen many other decays of Higgs, too, most recently even evidence for $H\\to\\mu^+\\mu^-$\n",
    "* In particular, selection here requires one of the taus to decay into an electron or muon and two neutrinos, and the other into hadrons and a neutrino. \n",
    "* The challenge is based on Monte Carlo collision events processed through the **[ATLAS detector](http://atlas.cern/)** simulation and reconstruction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## LHC and ATLAS\n",
    "* LHC collides bunches of protons every 25 nanoseconds inside ATLAS detector\n",
    "* In the hard-scattering process, two colliding protons interact and part of the kinetic energy of the protons is converted into new particles.\n",
    "* Most resulting particles are unstable and short-lived → decay quickly into a cascade of lighter particles.\n",
    "* ATLAS detector measures the properties of the decay products: type, energy and momentum (3-D direction)\n",
    "* The decay products are identified and reconstructed from the low-level analogue and digital signals they trigger in the detector hardware.\n",
    "* Part of the energy will be converted into and carried away by neutrinos (e.g. from the decay of tau leptons, $\\tau^- \\to e^- \\nu_\\tau \\bar\\nu_e$) that cannot be measured, leading to an incomplete event reconstruction and an imbalance in the total transverse momentum.\n",
    "\n",
    "Some event displays that visualize collision events found in real data that show a signature matching a $H\\to\\tau\\tau$ decay can be found on the [public ATLAS page][1]. [This event][2], for example, shows $H\\to\\tau\\tau$ with one tau lepton further decaying leptonically and the other hadronically.\n",
    "\n",
    "[1]: https://twiki.cern.ch/twiki/bin/view/AtlasPublic/EventDisplaysFromHiggsSearches#H_AN1\n",
    "[2]: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2012-160/figaux_07.png"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import urllib\n",
    "\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = \"atlas-higgs-challenge-2014-v2.csv.gz\"\n",
    "url = \"http://opendata.cern.ch/record/328/files/atlas-higgs-challenge-2014-v2.csv.gz\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists(filename):\n",
    "    urllib.request.urlretrieve(url, filename)\n",
    "df = pd.read_csv(filename)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Dataset\n",
    "The data contains > 800 k simulated collision events, that were used in the reference [ATLAS analysis][1]:\n",
    "* 250 k for training\n",
    "* 100 k for testing (public leaderboard)\n",
    "* 450 k for testing (private leaderboard)\n",
    "* a small withheld dataset\n",
    "\n",
    "Here, we use the full dataset:\n",
    "\n",
    "[1]: http://cds.cern.ch/record/1632191"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby(\"KaggleSet\").count()[\"EventId\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset mixes background (b) and signal (s) events:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby(\"Label\").size()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the actual $s:b$ ratio were so large ($\\sim1/3$), we would have found the Higgs much earlier. \n",
    "To obtain the actual number of signal and background events we expect in the 2012 ATLAS dataset, we need to take into account the provided weights:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby(\"Label\").Weight.sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is, without any additional selection we expect a signal-background ratio of only 1.7 permille.\n",
    "\n",
    "Each simulated event has a weight\n",
    "* proportional to the conditional density divided by the instrumental density used by the simulator (an importance-sampling flavor),\n",
    "* and normalized for integrated luminosity (the size of the dataset; factors in cross section, beam intensity and run time of the collider)\n",
    "\n",
    "The weights are an artifact of the way the simulation works and not part of the input to the classifier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# different weights correspond roughly to different background processes (due to the different cross sections)\n",
    "ax = plt.hist(df[\"Weight\"], bins = 100)\n",
    "plt.yscale('log')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Only three different background processes were retained in this dataset ($Z\\to\\tau\\tau$, top-quark-pair production, $W\\to\\ell\\nu$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.describe().T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Brief overview of variables, there is more information in the documentation. \n",
    "* 30 features\n",
    "  * The variables that start with **DER** are derived quantities, determined by the physicists performing the analysis as variables that discriminate signal from background. \n",
    "  * On the other hand, those that start with **PRI** are considered to be primary variables, from which the derived variables are calculated. \n",
    "    * They themselves generally do not provide much discrimination.\n",
    "    * One of the ideas suggested by deep networks is that they can determine the necessary features from the primary variables, potentially even finding variables that the physicists did not consider. \n",
    "* *EventId* identifies the event but is not a \"feature.\" \n",
    "* The *Weight* is the event weight.\n",
    "  * used to obtain the proper normalization of the different signal and background samples\n",
    "  * sum of weights of all signal events should produce the signal yield expected to be observed in 2012 LHC data taking\n",
    "  * sum of weights of all background events should produce the background yield\n",
    "* *Label* indicates if it is a signal or background event. \n",
    "* Ignore the *Kaggle* variables --- they are only used if you want to reproduce exactly what was used in the Challenge. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Investigate/visualize some parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feat_columns = [col for col in df.columns if col[:3] in [\"DER\", \"PRI\"]]\n",
    "len(feat_columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(ncols=5, nrows=6, figsize=(15, 10))\n",
    "for ax, col in zip(axs.ravel(), feat_columns):\n",
    "    kwargs = dict(bins=100, histtype=\"step\", density=True)\n",
    "    ax.hist(df[col][df.Label == \"s\"], **kwargs)\n",
    "    ax.hist(df[col][df.Label == \"b\"], **kwargs)\n",
    "    ax.set_xlabel(col)\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not all variables are defined in each event, that's where the `-999` values are used. This happens e.g. for the leading and subleading jet and quantities derived from jets in events where there is no jet or only one jet. So it makes sense to look at the distributions of values that are not `-999`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(df):\n",
    "    fig, axs = plt.subplots(ncols=5, nrows=6, figsize=(15, 10))\n",
    "    for ax, col in zip(axs.ravel(), feat_columns):\n",
    "        kwargs = dict(bins=100, histtype=\"step\", density=True)\n",
    "        x = df[col].to_numpy()\n",
    "        mask = x != -999\n",
    "        x = x[mask]\n",
    "        label = df.Label[mask].to_numpy()\n",
    "        ax.hist(x[label == \"s\"], **kwargs)\n",
    "        ax.hist(x[label == \"b\"], **kwargs)\n",
    "        ax.set_xlabel(col)\n",
    "    fig.tight_layout()\n",
    "\n",
    "plot(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes it can be instructive to look at 2D plots. The `seaborn` library provides a few helpers for this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# take sub-set of vars for plotting\n",
    "varplot = ['DER_mass_MMC', \n",
    "           'DER_mass_jet_jet',\n",
    "           'DER_deltar_tau_lep',\n",
    "           'DER_pt_tot',\n",
    "           'PRI_jet_subleading_pt']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we only plot events where no variable is -999. Also we only take a random subsample of 10000 events to have reasonable scatterplots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.pairplot(\n",
    "    df[~(df == -999).T.any()].sample(10000),\n",
    "    diag_kind=\"hist\",\n",
    "    vars=varplot,\n",
    "    hue=\"Label\",\n",
    "    markers=\".\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Machine learning\n",
    "\n",
    "Here we have a supervised ML problem where we want to fit a model that predicts from inputs `X` some labels `y`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = df.loc[:, feat_columns]\n",
    "y = df['Label']\n",
    "weight = df['Weight']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also here we will use splitting into training and testing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    X_train,\n",
    "    X_test,\n",
    "    y_train,\n",
    "    y_test,\n",
    "    weight_train,\n",
    "    weight_test,\n",
    ") = train_test_split(\n",
    "    X.to_numpy(),\n",
    "    (y == \"s\").to_numpy(),\n",
    "    weight.to_numpy(),\n",
    "    test_size=0.33,\n",
    "    random_state=42\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First ML trials w/ simple models\n",
    "1st attempt with simple models: GaussianNB and Logistic Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### GaussianNB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# GaussianNB (Gaussian Naive Bayes, assumes each class is drawn from an axis-aligned Gaussian distribution)\n",
    "from sklearn.naive_bayes import GaussianNB\n",
    "\n",
    "model_gnb= GaussianNB()\n",
    "model_gnb.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_gnb.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Logistic Regression\n",
    "As next attempt, let's look at [logistic regression][1]. This is a very simple, linear model. In the exercises you can look at optimizing it a bit more.\n",
    "* logistic function: $f(x) = \\frac{1}{1+\\exp(-x)}$, $f(x): [-\\infty,\\infty] \\to [0,1]$\n",
    "* model: $y = f(\\vec{x} \\cdot \\vec{\\beta} + \\epsilon)$\n",
    "\n",
    "[1]: https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_lr = LogisticRegression()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_lr.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h3>Exercise 3</h3>\n",
    "    Likely the model showed convergence issues. Can you fix this?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_lr.score(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_lr.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More sophisticated model: [GradientBoostingClassifier][1]\n",
    "* ensemble method that combines multiple decision trees\n",
    "* \"forward stage-wise fashion: each tree tries to correct the mistakes of the previous one (steered by the `learning_rate`)\n",
    "* individual trees can be simple (shallow), idea is to combine many \"weak learners\" \n",
    "  * each tree can only provide good predictions on part of the data, but combined they can yield a powerful model\n",
    "  \n",
    "[1]: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.ensemble import GradientBoostingClassifier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To speed up the training we will use subsampling (\"stochastic gradient boosting\") here - train each tree on a random subset of the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_gbc = GradientBoostingClassifier(subsample=0.01, verbose=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_gbc.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_gbc.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternative for larger datasets: [`HistGradientBoostingClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.HistGradientBoostingClassifier.html)\n",
    "\n",
    "This also trains sufficiently fast on a dataset this size, even without subsampling:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.ensemble import HistGradientBoostingClassifier\n",
    "\n",
    "model_gbc = HistGradientBoostingClassifier()\n",
    "model_gbc.fit(X_train, y_train)\n",
    "model_gbc.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to evaluate and compare performance?\n",
    "\n",
    "While the Accuracy can be useful to compare different methods on the same dataset it has often a bit limited information in typical particle-physics classification applications because we often want to distinguish rare signals from large background. Most classifiers provide a continuous output score where we can choose a tradeoff between **true positives** (our retained signal)  and **false positives** (the background that's still left over).\n",
    "\n",
    "This tradeoff is characterized by the so called **ROC curve** ([Receiver Operating Characteristic](https://en.wikipedia.org/wiki/Receiver_operating_characteristic))\n",
    "\n",
    "For example, our GradientBoostingClassifier gives predicted probabilities for both classes (first column background, second column signal):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_gbc.predict_proba(X_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They are typically normalized such that $p_\\mathrm{signal} = 1-p_\\mathrm{background}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p_test = model_gbc.predict_proba(X_test)[:, 1] # predicted probability for signal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visually get an idea of the separation power we can plot the distribution of predicted probabilities for both classes separately:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_proba(p, y):\n",
    "    kwargs = dict(bins=100, range=(0, 1), histtype=\"step\")\n",
    "    plt.hist(p[y==0], label=\"background\", **kwargs)\n",
    "    plt.hist(p[y==1], label=\"signal\", **kwargs)\n",
    "    plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_proba(p_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... but one should be a bit careful interpreting these by eye, since they can be a bit misleading.\n",
    "\n",
    "Especially for comparing different classifiers, the ROC curve is a better option:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import roc_curve"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fpr, tpr, thr = roc_curve(y_test, p_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(fpr, tpr)\n",
    "plt.xlabel(\"false positive rate (Background efficiency)\")\n",
    "plt.ylabel(\"true positive rate (Signal efficiency)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We get this curve by looking at every possible cutoff value on the predicted probability:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(thr, tpr, label=\"true positive rate (Signal efficiency)\")\n",
    "plt.plot(thr, fpr, label=\"false positive rate (Background efficiency)\")\n",
    "plt.xlabel(r\"$p_\\mathrm{pred}$ threshold\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When high background rejection is important (as often in particle physics) it's also common to plot the true positive rate against the inverse of the false positive rate in logarithmic scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tpr, 1 / fpr)\n",
    "plt.yscale(\"log\")\n",
    "plt.xlabel(\"Signal efficiency\")\n",
    "plt.ylabel(\"Background rejection\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h3>Exercise 4:</h3> Compare the ROC curve for the Logistic Regression, Gaussian Naive-Bayes and the Gradient Boosting Classifier\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Figure-of-Merit: AMS\n",
    "Let's get back to the original problem using the GradientBoostingClassifier. The Kaggle competition used the approximate median significance ([AMS][1]), as defined below, to determine how good a solution was. \n",
    "The goal is to maximize signal and minimize background, and the AMS is an approximate formula to quantify the expected signal significance.\n",
    "\n",
    "The formula is based on the result from [arXiv:1007.1727](https://arxiv.org/pdf/1007.1727) which approximates the **discovery significance** (corresponding to the p-value for rejecting the background-only hypothesis) using Wilk's theorem in a counting experiment:\n",
    "\n",
    "$Z_{0, A} = \\sqrt{q_{0, A}} = \\sqrt{2((s+b)\\ln(1+s/b)-s)}$\n",
    "\n",
    "For large $b$ this converges to the intuitive rule-of-thumb formula $\\frac{s}{\\sqrt{b}}$.\n",
    "\n",
    "[1]: https://github.com/nikoladze/LMU_DA_ML/blob/master/AMS.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ams(s, b):\n",
    "    \"\"\"\n",
    "    Approximate median significance, as defined in Higgs Kaggle challenge\n",
    "\n",
    "    The number 10, added to the background yield, is a regularization term to decrease the variance of the AMS.\n",
    "    \"\"\"\n",
    "    return np.sqrt(2 * ((s + b + 10) * np.log(1 + s / (b + 10)) - s))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate this we need the **absolute normalization** of signal and background. So we need to **sum of weights** for both:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sumw = df.groupby(\"Label\").Weight.sum()\n",
    "sumw"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nsig_tot = sumw[\"s\"]\n",
    "nbkg_tot = sumw[\"b\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using this we can get the AMS value after applying a threshold cut, e.g. for taking all events with predicted probability > 50%:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = p_test > 0.5\n",
    "nsig = weight_test[y_test == 1].sum()\n",
    "nsig_sel = weight_test[mask][y_test[mask] == 1].sum()\n",
    "tpr = nsig_sel / nsig\n",
    "nbkg = weight_test[y_test == 0].sum()\n",
    "nbkg_sel = weight_test[mask][y_test[mask] == 0].sum()\n",
    "fpr = nbkg_sel / nbkg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ams(s=tpr*nsig_tot, b=fpr*nbkg_tot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h3>Exercise 5:</h3> Plot the AMS against every possible threshold and find the maximum. <b>Hint:</b> Use <code>roc_curve</code> with <code>sample_weight=weight_test</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How well did we do? Compare to results of Kaggle challenge:\n",
    "\n",
    "![](figures/tr150908_davidRousseau_TMVAFuture_HiggsML.001.png)\n",
    "\n",
    "Note: The *private* data set/Leaderboard on Kaggle is a test data where the scores are only revealed to the participants at the end of the challenge."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Applying sample weights during training\n",
    "\n",
    "Knowing that we need to weight the samples in the end we can take this into account in the training for classifiers that support this. This works well for probabilistic classifiers with a loss function, e.g. GradientBoosting or Neural Networks. However, just applying the weights may lead to problems since\n",
    "\n",
    "* They are not around 1 (shifting the total scale of the loss)\n",
    "* They are much smaller for the signal (due to the lower cross section) than for the background\n",
    "\n",
    "To deal with this we scale the weights such that our signal and background classes have the same sum of weights and finally such that the mean weight is 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class_weight = np.array([\n",
    "    len(y_train) / weight_train[y_train==0].sum(),\n",
    "    len(y_train) / weight_train[y_train==1].sum(),\n",
    "])\n",
    "class_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "weight_for_fit = weight_train * class_weight[y_train.astype(int)]\n",
    "weight_for_fit /= weight_for_fit.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h3>Exercise 6:</h3> The <code>HistGradientBoostingClassifier</code> support weights during training via the <code>sample_weight</code> argument in the <code>fit</code> method. Can you improve by applying the weight?\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
